#include <cmath>
#include <vector>
#include <numeric>
#include <iostream>
#include <string>
#include <fstream>

std::vector<double> readData(const std::string& path) {
  std::ifstream file;
  file.open(path);

  std::vector<double> result;

  double entry;
  while (file >> entry) {
    result.push_back(entry);
  }

  file.close();

  return result;
}

double squareError(double x, double y) {
  return std::pow(x - y, 2);
}

double rmse(const std::vector<double>& target,
            const std::vector<double>& pred) {
  double sum = std::transform_reduce(target.begin(), target.end(), pred.begin(),
                                     0, std::plus<double>(), squareError);
  return std::sqrt(sum / target.size());
}

int main() {
  auto predictiction = readData("data/predictions.txt");
  auto target = readData("data/y_test.txt");

  std::cout << "rmse: " << rmse(target, predictiction) << std::endl;
}